import sys
import string


def rot(cryptotext, rot_value=13):
    lowers = string.maketrans(string.lowercase, string.lowercase[rot_value:] + string.lowercase[:rot_value])
    uppers = string.maketrans(string.uppercase, string.uppercase[rot_value:] + string.uppercase[:rot_value])
    return cryptotext.translate(lowers).translate(uppers)


if __name__ == '__main__':
    print rot(sys.argv[1].strip())
