import sys

from rot import rot


def ceasar(ciphertext):
    return [rot(ciphertext, i) for i in xrange(0, 26)]


if __name__ == '__main__':
    for i, m in enumerate(ceasar(sys.argv[1].strip())):
        print i, m
