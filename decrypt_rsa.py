import math
import fractions
import sys


def decrypt(p, q, e, c):
    print "p: {}\nq: {}\ne: {}\nc: {}".format(p, q, e, c)
    n = p * q
    print "n: {}".format(n)
    totient_n = lcm(p - 1, q - 1)
    print "totient(n): {}".format(totient_n)
    d = mmi(e, totient_n)
    print "d: {}".format(d)
    return pow(c, d, n)


def mmi(a, m):
    """
    Given a and m, calculates x when
    x * a congruent 1 (mod m)
    """
    g, x, y = egcd(a, m)
    return x % m


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def lcm(a, b):
    return int(math.fabs((float(a)) / fractions.gcd(a, b)) * math.fabs(float(b)))


if __name__ == '__main__':
    p = int(sys.argv[1].strip())
    q = int(sys.argv[2].strip())
    e = int(sys.argv[3].strip())
    c = int(sys.argv[4].strip())
    print decrypt(p, q, e, c)
