import string
import sys


def vigenere(cryptotext, key):
    full_key = ""
    if len(cryptotext) != len(key):
        if len(cryptotext) % len(key) == 0:
            # Just a multiplication
            full_key = key * (len(cryptotext) / len(key))
        else:
            full_key = key * (len(cryptotext) / len(key)) + key[:len(cryptotext) % len(key)]
    else:
        full_key = key

    plaintext = ""
    for c, k in zip(cryptotext, full_key):
        index = (string.uppercase.find(c.upper()) + string.uppercase.find(k.upper())) % len(string.uppercase)
        plaintext += string.uppercase[index]

    return plaintext


if __name__ == '__main__':
    print vigenere(sys.argv[1].strip(), sys.argv[2].strip())
