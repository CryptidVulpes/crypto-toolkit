import sys
import string
from Queue import PriorityQueue

import frequency_analyzer


def xor_cipher(cryptotext, key):
    return ''.join([chr(ord(a) ^ ord(b)) for a, b in zip(cryptotext, key)])


def get_etaoin_score(text):
    a = frequency_analyzer.find_letter_counts(text)
    b = frequency_analyzer.compose_frequency_order_string(a)
    return frequency_analyzer.get_etaoin_score(b)


def brute_xor(cryptotext, start, end):
    top_result = ""
    top_score = 0
    top_key = ""
    top3 = PriorityQueue(3)
    for byte in xrange(start, end):
        plaintext = xor_cipher(cryptotext, chr(byte) * len(cryptotext))
        score = get_etaoin_score(plaintext)
        if score > top_score:
            top_score = score
            top_result = plaintext
            top_key = chr(byte)
        if top3.full():
            top3.get_nowait()
        top3.put((score, plaintext))

    return top_key, top_score, top_result, top3


if __name__ == '__main__':
    cipher = sys.argv[1].strip()
    start_byte = int(sys.argv[2].strip())
    end_byte = int(sys.argv[3].strip())
    top_key, top_score, top_result, top3 = brute_xor(cipher, start_byte, end_byte)
    print top_key, top_score, top_result
    print [top3.get_nowait() for _ in xrange(3)]
