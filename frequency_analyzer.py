import sys
import collections
import string
from pprint import pformat


ETAOIN = 'ETAOINSHRDLCUMWFGYPBVKJXQZ'
LETTERS = string.uppercase


def find_letter_counts(text):
    freq_count = {k: 0 for k in LETTERS}
    freq_count.update(dict(collections.Counter(text)))
    return freq_count


def find_relative_letter_counts(text):
    return {k: float(v) / float(len(text)) for k, v in find_letter_counts(text).items()}


def compose_frequency_order_string(letter_counts):
    return "".join(sorted(letter_counts, key=letter_counts.get, reverse=True))


def get_etaoin_score(frequency_order_string):
    first_six = (frequency_order_string[:6], ETAOIN[:6])
    last_six = (frequency_order_string[-6:], ETAOIN[-6:])

    top_score = sum([a in b for a in first_six[0] for b in first_six[1]])
    bottom_score = sum([a in b for a in first_six[0] for b in first_six[1]])
    return top_score + bottom_score


def get_original_case_string(uppercase, original):
    mix_case_decrypted = ""
    for c, m in zip(original, uppercase):
        if c.islower():
            mix_case_decrypted += m.lower()
        else:
            mix_case_decrypted += m

    return mix_case_decrypted


if __name__ == '__main__':
    cr = sys.argv[1].strip()
    print "Got: {}".format(cr)

    letter_counts = find_letter_counts(cr.upper())
    print "Letter counts: {}".format(pformat(letter_counts))

    relative_letter_counts = find_relative_letter_counts(cr.upper())
    print "Relative letter counts: {}".format(pformat(relative_letter_counts))

    letters_in_frequency_order = compose_frequency_order_string(letter_counts)
    print "Frequency order string: {}".format(letters_in_frequency_order)

    etaoin_trans = string.maketrans(letters_in_frequency_order, ETAOIN)
    decrypted = cr.upper().translate(etaoin_trans)

    mix_case_decrypted = get_original_case_string(decrypted, cr)
    print "Translated to ETAOIN order: {}".format(mix_case_decrypted)
